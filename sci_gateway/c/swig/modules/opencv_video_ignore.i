// Scilab Computer Vision Module
// Copyright (C) 2017 - Scilab Enterprises

%ignore CvKalman;
%ignore DenseOpticalFlow;
%ignore cvCalcOpticalFlowFarneback;
%ignore cvEstimateRigidTransform;
%ignore cvUpdateMotionHistory;
%ignore cvCalcMotionGradient;
%ignore cvCalcGlobalOrientation;
%ignore cvSegmentMotion;
%ignore cvCamShift;
%ignore cvMeanShift;
