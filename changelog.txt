// Scilab Computer Vision Module
// Copyright (C) 2017 - Scilab Enterprises
// Copyright (C) 2025 - Dassault Systèmes S.E. - Vincent COUVERT

changelog of the Scilab Computer Vision Module

0.1: initial version - prototype

0.2: first release - alpha1 version

0.3: public release - alpha2 version
    - fixed PNG issue on some Linux distributions
    - added MacOSx support
    - added demos, help pages
    - implemented size & extraction operators
    - fixed tests
    - 3rd party libraries (OpenCV/FFMPEG) rebuilt to be not GPL licensed

0.4: alpha3 release
    - bad side effects fixed (#1688)
    - some memory leaks fixed
    - documentation added
    - issues #1694, #1690

2.4.13:
    - Swig: 4.3.0
    - Linux: OpenCV 2.4.13.6 / FFmpeg 3.4.9
    - macOS: OpenCV 2.4.13.6 / FFmpeg 3.4.9
    - Windows: OpenCV 2.4.13.6 / FFmpeg 4.2.3

4.8.1:
    - Swig: 4.3.0
    - Linux: OpenCV 4.8.1 / FFmpeg 7.1
    - macOS: OpenCV 4.8.1 / FFmpeg 7.1
    - Windows: OpenCV 4.8.1
