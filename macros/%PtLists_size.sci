// Scilab Computer Vision Module
// Copyright (C) 2017 - Scilab Enterprises

function ret = %PtLists_size(ptLists, varargin)
    ret = cvGetPtListsSize(ptLists);
endfunction
