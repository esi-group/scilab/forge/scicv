// Scilab Computer Vision Module
// Copyright (C) 2025 - Dassault Systèmes S.E. - Vincent COUVERT

OPENCV_VERSION = "4.8.1"

FFMPEG_VERSION = "7.1" // Latest version

OPENH264_VERSION = "1.8.0"  // Version imposed by FFmpeg 

