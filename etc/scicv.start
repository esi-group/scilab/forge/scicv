// Scilab Computer Vision Module
// Copyright (C) 2017 - Scilab Enterprises
// Copyright (C) 2025 - Dassault Systèmes S.E. - Vincent COUVERT

function [scicvlib] = startModule()

  TOOLBOX_NAME  = "scicv";
  TOOLBOX_TITLE = "Scilab Computer Vision Module";

  mprintf("Start " + TOOLBOX_TITLE + "\n");

  if isdef("scicvlib") then
      warning("Scilab Computer Vision Module is already loaded");
      return;
  end

  etc_tlbx  = get_absolute_file_path("scicv.start");
  etc_tlbx  = getshortpathname(etc_tlbx);
  tlbx_root_dir = strncpy(etc_tlbx, length(etc_tlbx)-length("\etc\"));

// Load macros
// =============================================================================
  mprintf("\tLoad macros\n");
  pathmacros = fullfile(tlbx_root_dir, "macros");
  scicvlib = lib(pathmacros);

// Load thirdparties
// =============================================================================
  verboseMode = ilib_verbose();
  ilib_verbose(0);

  mprintf("\tLoad thirdparties\n");
  [version, opts] = getversion();
  arch = opts(2);

  opencv_libs = "opencv_world";
  os = getos();

  if os == "Windows" then
      bin_dir = fullfile(tlbx_root_dir, "thirdparty", os, arch, "bin");
      ffmpeg_libs = [];
      opencv_ffmpeg_libs = ["opencv_videoio_ffmpeg481_64"; "openh264-1.8.0-win64"];
      opencv_libs = [opencv_libs + "481"];
      // load debug library when needed
			if grep(opts, "debug") then
        opencv_libs = opencv_libs + "d";
      end
      thirdparty_libs = [ffmpeg_libs; opencv_ffmpeg_libs; opencv_libs];
      thirdparty_lib_paths = fullpath(bin_dir + "\" + thirdparty_libs + ".dll");
  else
      lib_dir = fullfile(tlbx_root_dir, "thirdparty", os, arch, "lib");
      video_codec_libs = ["openh264"];
      ffmpeg_libs = ["avutil"; "swscale"; "swresample"; "avcodec"; "avformat"; "avfilter"; "avdevice"];
      thirdparty_libs = [video_codec_libs; ffmpeg_libs; opencv_libs];
      thirdparty_lib_paths = fullpath(lib_dir + "/" + "lib" + thirdparty_libs + getdynlibext());
  end
  try
    for thirdparty_lib_path = thirdparty_lib_paths'
      link(thirdparty_lib_path);
    end
  catch
    error(msprintf("\tUnable to link %s", thirdparty_lib_path));
  end

// Load gateways
// =============================================================================
  mprintf("\tLoad gateways\n");
  exec(fullfile(tlbx_root_dir, "sci_gateway/loader_gateway.sce"));
  ilib_verbose(verboseMode);

// Load demos
// =============================================================================
  if or(getscilabmode() == ["NW";"STD"]) then
    mprintf("\tLoad demos\n");
    exec(fullfile(tlbx_root_dir, "demos/scicv.dem.gateway.sce"));
  end

// Load localization
// =============================================================================
  addlocalizationdomain(TOOLBOX_NAME, fullfile(tlbx_root_dir, "locales"));

// Load help
// =============================================================================
  if or(getscilabmode() == ["NW";"STD"]) then
      mprintf("\tLoad help\n");
      path_addchapter = fullfile(tlbx_root_dir, "jar");
      if isdir(path_addchapter) then
          add_help_chapter(TOOLBOX_NAME, path_addchapter, %F);
      end
  end
endfunction

[scicvlib] = startModule();
clear startModule;
